# Actions nécessaires pour organiser une conférence

[[_TOC_]]

## Programmation
### Créneaux
1. Présentations
    - Courtes : 20 minutes
    - Normales : 45 minutes
2. Ateliers
    - Normaux : 1h20 à 2 heures
    - Longs : 3 heures et plus
### Sujets
- Niveau : avancé, débutant, bases nécessaires
- Type : technique, techno, REX
### Oratrice - Orateur
- Mixité
- Francophone ou non
- Provenance
### Remplacement
Qui, quoi comment !
### Chronologie
```mermaid
timeline
title Oratrice - orateur
  section Avant la conférence
  Au plus tôt : Recrutement (CFP ou pas) : Proposer du mentorat
  Tout le temps : Accompagnement : Répétition : Peaufinage
  - 1 mois : Transport et hébergement
  J - 1 : Arrivée sur place : Soirée speakers
  section Jour de la conférence
  Journée : Accueil : Vérification matériel
  section Après la conférence
  J + 7 : Feedback : Partage lien vidéo
```
## Salle
### Services
- Pause permanente
    - Petit déjeuner
    - Boissons chaudes ou froides
    - Service
- Securité
- Vestiaire
- Matériel de captation
- Matériel de projection
- Mobilier atelier (bureaux, chaises, tableaux)

### Disposition
- /!\ son
- Bruit entre les salles

### Emplacement
- Accès transport en commun
- Proximité gares

### Chronologie
```mermaid
timeline
title Salle
  section Avant la conférence
  - 3 mois : Visite : Accompte
  - 1 mois : Plan : Communication graphismes
  J - 7 : Confirmation quantité
  section Jour de la conférence
  Journée : Orchestration : Installation : Accueil
  section Après la conférence
  J + 7 : Feedback
```

## Traiteur
### Déjeuner
Prévoir des menus pour tout le monde :
- sans restriction
- végan (embarque les végés)
- sans gluten (demander à la prérarer minute pour gérer végé ou pas)

Logistique :
- serviettes
- déchets

### Apéro
- Avec et sans alcool
- si fromage et charcuterie : les présenter séparement

### Chronologie
```mermaid
timeline
title Traiteur
  section Avant la conférence
  - 1 mois : Validation menu
  J - 7 : Confirmation quantité
  section Jour de la conférence
  Journée : Orchestration : Installation : Accueil
  section Après la conférence
  J + ? : Paiement
```

## Sponsors
- **/!\ Très chronophage /!\ **
- Intéret
- Transparence
- Clarifier les objectifs
### Packs
- Temps de parole
- Affichage
- Stand
- Créneau « réservé »
- Équité entre les packs
### Actions marketing communication
- Annonce sur les réseaux sociaux
### Chronologie
```mermaid
timeline
title Sponsors
  section Avant la conférence / suivi / échanges
  Au plus tôt : Identification
  - 3 mois : Définition des packs
  - 1 mois : Facturation
  section Jour de la conférence
  Journée : Orchestration : Installation : Accueil
  section Après la conférence
  J + ? : Feedback : Remerciements
```

## Soirée speakers
- Fort moment de convivialité très attendu
- Permet de remercier les bénévoles
- Simple d’organisation si l’on reste en mode bonne franquette
- Permet parfois quelques fignolage de dernière minute

## Captation

C’est un élément très important quand la réputation de la conférence (par exemple les vidéos de la halle de xcraft c’est 4000 vue à fin décembre 2023). Ça permet aussi aux speakers d’être pris dans d’autres conférences.

Ça demande des compétences spécifiques et du temps pour le montage.

### Priorisation
Qualité du son > captation écran > caméra scéne > son ambiance

### Matériel
- Si table de mixage, connaître le modèle et avoir le manuel est vital.
- Arriver tôt pour faire les branchements et les essais
- Prévoir de la cablerie en tout genre
- PC dédié captation (carte son)
- caméra
- Stream deck
- Serre-tête > micro-cravatte > micro classique

### Ordonnancement
- Avoir du temps de bascule entre speaker
- l’installation peut ne pas fonctionner (problème de switch hdmi, surchauffe caméra)
- Avoir plus d’une personne à la régie est une bonne chose
- Ouvrir le micro question dans les temps

## Fournitures
- Post-it moyens et grands
- Stylos et feutres
- scotch peintre
- tableau
- papier pour coller les post-its
- Étiquettes pour les noms

## Actions de la journée
Difficile de planifier quelque chose qui convient à l’ensemble des bénévoles (qui veut voir quoi), des actions surprises peuvent être chronophage (du genre choper des post-its dans le centre commercial d’à côté).

- Accueil des speakers
- Guichet (lutte contre la gruge ???)
- Régie
- Facilitation salle (question du public)
- Intéraction avec la salle pour la petite logistique
- Accueil traiteur

## Feedback
- Openfeedback un peu fastidieux à préparer mais très pratique
- Questionnaire à préparer en avance pour l’envoyer aux convives le lendemain de la conf
- Discussions à chaud ou à froid

Essayer d’être constructif et adapter les choses en étant pragmatique.